import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { MatConfirmDialogComponent } from './components/mat-confirm-dialog/mat-confirm-dialog/mat-confirm-dialog.component';
import { Error404Component } from './components/error404/error404.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    MatConfirmDialogComponent,
    Error404Component,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
  ]
})
export class SharedModule { }
