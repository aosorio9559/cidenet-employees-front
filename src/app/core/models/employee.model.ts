export interface Employee {
  firstName: string;
  secondName: string;
  lastName: string;
  secondLastName: string;
  country: string;
  idType: string;
  idNumber: string;
  email: string;
  entranceDate: Date;
  area: string;
  status: string;
  registerDate: string;
}