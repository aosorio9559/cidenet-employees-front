import { Employee } from './employee.model';

export interface EmployeeApi {
  ok: boolean,
  data?: Employee[];
  msg?: string;
}