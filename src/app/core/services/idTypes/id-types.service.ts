import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IdType } from '../../models/idType.model';

@Injectable({
  providedIn: 'root'
})
export class IdTypesService {
  url: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAllIdTypes() {
    return this.http.get<IdType[]>(`${this.url}/idTypes`);
  }
}
