import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpErrorInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      /* Client-side or network error */
      console.error('Ha ocurrido un error:', error.error);
    } else {
      /* Backend error */
      console.error("Código retornado por el backend:", error.status);
      console.error("Cuerpo del error:", error.error);
    }
    /* Show error message */
    return throwError("Ha ocurrido un error");
  }
}
