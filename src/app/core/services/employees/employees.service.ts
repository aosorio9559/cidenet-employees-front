import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { EmployeeApi } from '../../models/api-employee.model';
import { Employee } from '../../models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  url: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAllEmployees() {
    return this.http.get<Employee[]>(`${this.url}/employees`);
  }

  getEmployee(id: string) {
    return this.http.get<Employee>(`${this.url}/employees/${id}`);
  }

  createEmployee(employee: Employee) {
    return this.http.post(`${this.url}/employees`, employee);
  }

  updateEmployee(employee: Employee, id: string) {
    return this.http.put(`${this.url}/employees/${id}`, employee);
  }

  deleteEmployee(id: string) {
    return this.http.delete<EmployeeApi>(`${this.url}/employees/${id}`);
  }
}
