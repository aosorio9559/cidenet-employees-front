import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { EmployeeApi } from '../../models/api-employee.model';
import { Area } from '../../models/area.model';

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  url: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAllAreas() {
    return this.http.get<Area[]>(`${this.url}/areas`);
  }
}
