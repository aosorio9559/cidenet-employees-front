import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Status } from '../../models/status.model';

@Injectable({
  providedIn: 'root'
})
export class StatusService {
  url: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAllStatus() {
    return this.http.get<Status[]>(`${this.url}/status`);
  }
}
