import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesService } from './services/employees/employees.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [EmployeesService]
})
export class CoreModule { }
