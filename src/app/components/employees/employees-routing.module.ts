import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateEmployeeComponent } from './components/create-employee/create-employee.component';
import { ListEmployeesComponent } from './components/list-employees/list-employees.component';


const routes: Routes = [
  {
    path: "",
    component: ListEmployeesComponent
  },
  {
    path: "create",
    component: CreateEmployeeComponent
  },
  {
    path: "create/:id",
    component: CreateEmployeeComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
