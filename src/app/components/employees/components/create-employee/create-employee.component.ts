import { Component, OnInit } from "@angular/core";
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute } from "@angular/router";
import { map } from 'rxjs/operators';
import { Area } from "src/app/core/models/area.model";
import { Country } from "src/app/core/models/country.model";
import { Employee } from "src/app/core/models/employee.model";
import { IdType } from "src/app/core/models/idType.model";
import { Status } from "src/app/core/models/status.model";
import { AreasService } from "src/app/core/services/areas/areas.service";
import { EmployeesService } from "src/app/core/services/employees/employees.service";
import { IdTypesService } from "src/app/core/services/idTypes/id-types.service";
import { StatusService } from "src/app/core/services/status/status.service";
import { CountriesService } from '../../../../core/services/countries/countries.service';

@Component({
  selector: "app-create-employee",
  templateUrl: "./create-employee.component.html",
  styleUrls: ["./create-employee.component.scss"]
})

export class CreateEmployeeComponent implements OnInit {
  countries: Country[] = [];
  idTypes: IdType[] = [];
  areas: Area[] = [];
  status: Status[] = [];
  minEntranceDate: Date;
  maxEntranceDate: Date;
  employeeId: string;
  /** Asigna la fecha actual del sistema en formato `DD/MM/YYYY HH:mm:ss` */
  formattedRegisterDate = String(new Date().toLocaleString('en-GB')).replace(",", "");

  validatorValues = {
    shortMaxLength: 20,
    longMaxLength: 50,
    onlyLettersPattern: "[A-Z ]*"
  };

  createEmployeeForm = this.fb.group({
    firstName: ["", Validators.compose([
      Validators.required,
      Validators.maxLength(this.validatorValues.shortMaxLength),
      Validators.pattern(this.validatorValues.onlyLettersPattern),
      // this.removeWhitespaceValidator
    ])],
    secondName: ["", Validators.compose([
      Validators.maxLength(this.validatorValues.longMaxLength),
      Validators.pattern(this.validatorValues.onlyLettersPattern)
    ])],
    lastName: ["", Validators.compose([
      Validators.required,
      Validators.maxLength(this.validatorValues.shortMaxLength),
      Validators.pattern(this.validatorValues.onlyLettersPattern)
    ])],
    secondLastName: ["", Validators.compose([
      Validators.required,
      Validators.maxLength(this.validatorValues.shortMaxLength),
      Validators.pattern(this.validatorValues.onlyLettersPattern)
    ])],
    country: ["", Validators.required],
    idType: ["", Validators.required],
    idNumber: ["", Validators.compose([
      Validators.required,
      Validators.maxLength(this.validatorValues.shortMaxLength),
      Validators.pattern("[a-zA-Z0-9-]*")
    ])],
    email: ["", Validators.compose([
      Validators.required,
      Validators.email,
      Validators.maxLength(300),
    ])],
    entranceDate: ["", Validators.compose([
      Validators.required,
    ])],
    area: ["", Validators.required],
    status: [{ value: "Activo", disabled: true }, Validators.required],
    registerDate: [{
      value: this.formattedRegisterDate, disabled: true
    }, Validators.required],
  });

  get employeeForm() {
    return this.createEmployeeForm.controls;
  }

  constructor(
    private fb: FormBuilder,
    private employeesService: EmployeesService,
    private areasService: AreasService,
    private countriesService: CountriesService,
    private idTypesService: IdTypesService,
    private statusService: StatusService,
    private route: ActivatedRoute,
    private actionResultSnackBar: MatSnackBar
  ) {
    /* Se restan 30 días a la fecha actual del sistema */
    this.minEntranceDate = new Date(new Date().setDate(new Date().getDate() - 30));
    this.maxEntranceDate = new Date();
  }

  ngOnInit(): void {
    this.getAllAreas();
    this.getAllCountries();
    this.getAllIdTypes();
    this.getAllStatus();
    /* Recibe parámetro `id` desde la URL */
    this.route.params.subscribe(param => this.employeeId = param["id"]);

    if (this.employeeId) {
      this.getEmployee(this.employeeId);
    }

    this.createEmployeeForm.valueChanges
      .pipe(
        map(val => {
          val.countryCode = "";
          /* Se recorren los países retornados por el respectivo servicio para asignar el dominio del país al correo */
          if (val.country) {
            val.countryCode = this.countries.find(co => co.name === val.country).code;
          }
          return val;
        }))
      .subscribe(value => {
        const firstName = String(value.firstName).toUpperCase();
        const secondName = String(value.secondName).toUpperCase();
        const lastName = String(value.lastName).toUpperCase();
        const secondLastName = String(value.secondLastName).toUpperCase();
        const emailNames = `${firstName}.${lastName}`.toLowerCase().replace(/\s/g, "");
        const fullEmail = `${emailNames}@cidenet.com.${value.countryCode}`;

        this.employeeForm.firstName.setValue(firstName, { emitEvent: false });
        this.employeeForm.secondName.setValue(secondName, { emitEvent: false });
        this.employeeForm.lastName.setValue(lastName, { emitEvent: false });
        this.employeeForm.secondLastName.setValue(secondLastName, { emitEvent: false });

        if (firstName || lastName) {
          this.employeeForm.email.setValue(fullEmail, { emitEvent: false });
        } else {
          this.employeeForm.email.setValue("", { emitEvent: false });
        }
      });
  }

  getAllAreas() {
    this.areasService.getAllAreas().subscribe(areas => this.areas = areas);
  }

  getAllCountries() {
    this.countriesService.getAllCountries().subscribe(countries => this.countries = countries);
  }

  getAllIdTypes() {
    this.idTypesService.getAllIdTypes().subscribe(idTypes => this.idTypes = idTypes);
  }

  getAllStatus() {
    this.statusService.getAllStatus().subscribe(status => this.status = status);
  }

  getEmployee(id: string) {
    this.employeesService.getEmployee(id).subscribe(employee => {
      this.employeeForm.status.enable();
      /* Convierte el objeto retornado por el servicio en un array de arrays, permitiendo hacer el recorrido del mismo para asignar los valores (`value`) a los respectivos controles del formulario (`key`) */
      Object.entries(employee).forEach(([key, value]) => {
        if (this.employeeForm[key]) {
          this.employeeForm[key].setValue(value);
        }
      });
    });
  }

  registerOrUpdateEmployee() {
    if (this.createEmployeeForm.invalid) {
      this.validateAllFormFields(this.createEmployeeForm);
      return;
    }
    if (!this.employeeId) {
      this.createEmployee();
    } else {
      this.updateEmployee(this.employeeId);
    }
  }

  createEmployee() {
    const employee: Employee = this.createEmployeeForm.getRawValue();
    this.employeesService.createEmployee(employee).subscribe(() => {
      this.openActionResultSnackBar("Empleado registrado exitosamente", "Aceptar");
    });
  }

  updateEmployee(id: string) {
    const employee: Employee = this.createEmployeeForm.getRawValue();
    this.employeesService.updateEmployee(employee, id).subscribe(() => {
      this.openActionResultSnackBar("Empleado actualizado exitosamente", "Aceptar");
    });
  }

  removeWhitespaceValidator(control: AbstractControl) {
    if (control && control.value && !control.value.trim().length) {
      control.setValue("");
      return { required: true };
    }
    return null;
  }

  /** Permite hacer validaciones de controles de un formulario de forma recursiva
   * @param formGroup - FormGroup a validar
  */

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      } else if (control instanceof FormArray) {
        /* Valida los formGroups del FormArray en caso de que los hubiere */
        const formArrayControls = control.controls;
        formArrayControls.forEach(formArrayControl => {
          if (formArrayControl instanceof FormGroup) {
            this.validateAllFormFields(formArrayControl);
          }
        });
      }
    });
  }

  openActionResultSnackBar(message: string, action: string) {
    this.actionResultSnackBar.open(message, action, { duration: 5000 });
  }
}
