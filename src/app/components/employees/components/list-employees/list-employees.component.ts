import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EmployeesService } from 'src/app/core/services/employees/employees.service';
import { MatConfirmDialogComponent } from 'src/app/shared/components/mat-confirm-dialog/mat-confirm-dialog/mat-confirm-dialog.component';
import { Employee } from '../../../../core/models/employee.model';

@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.scss']
})
export class ListEmployeesComponent implements OnInit {
  employees: Employee[] = [];

  constructor(
    private employeesService: EmployeesService,
    public dialog: MatDialog,
    private actionResultSnackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.getAllEmployees();
  }

  getAllEmployees() {
    this.employeesService.getAllEmployees()
      .subscribe(employees => this.employees = employees);
  }

  openDeleteEmployeeDialog(id: string) {
    const dialogRef = this.dialog.open(MatConfirmDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.employeesService.deleteEmployee(id).subscribe(data => {
          if (data.ok) {
            this.openActionResultSnackBar("Empleado eliminado exitosamente", "Aceptar");
            this.getAllEmployees();
          }
        });
      }
    });
  }

  openActionResultSnackBar(message: string, action: string) {
    this.actionResultSnackBar.open(message, action, { duration: 5000 });
  }

}
