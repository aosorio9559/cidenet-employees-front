import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { Error404Component } from './shared/components/error404/error404.component';


const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "",
        redirectTo: "/employees",
        pathMatch: "full",
      },
      {
        path: "employees",
        loadChildren: () => import("./components/employees/employees.module").then(m => m.EmployeesModule)
      }
    ]
  },
  {
    path: "**",
    component: Error404Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
